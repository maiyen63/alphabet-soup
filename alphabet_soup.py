import random
import string

def create_alphabet_soup(rows, cols):
    """Generates a grid of random alphabet letters"""
    soup = [
        ['H', 'A', 'S', 'D', 'F'],
        ['G', 'E', 'Y', 'B', 'H'],
        ['J', 'K', 'L', 'Z', 'X'],
        ['C', 'V', 'B', 'L', 'N'],
        ['G', 'O', 'O', 'D', 'O']
]
   # for _ in range(rows):
       # row = [random.choice(string.ascii_uppercase) for _ in range(cols)]
       # soup.append(row)
    return soup

def print_soup(soup):
    """Prints the alphabet soup grid"""
    for row in soup:
        print(' '.join(row))

def find_word(soup, word):
    """Finds the word in the alphabet soup"""
    word_len = len(word)
    rows = len(soup)
    cols = len(soup[0])

 # Check horizontally and vertically
    for r in range(rows):
        for c in range(cols):
            if c + word_len <= cols and ''.join(soup[r][c:c+word_len]) == word:
                return True
            if r + word_len <= rows and ''.join([soup[r+i][c] for i in range(word_len)]) == word:
                return True

    # Check diagonally
    for r in range(rows):
        for c in range(cols):
            if r + word_len <= rows and c + word_len <= cols and ''.join([soup[r+i][c+i] for i in range(word_len)]) == word:
                return True
            if r - word_len >= -1 and c + word_len <= cols and ''.join([soup[r-i][c+i] for i in range(word_len)]) == word:
                return True

    return False

def main():
    # Define the size of the grid
    rows = 5  # Number of rows
    cols = 5  # Number of columns
   
    # Create the alphabet soup
    alphabet_soup = create_alphabet_soup(rows, cols)

    # Print the alphabet soup
    print_soup(alphabet_soup)

 # Words to find
    words = ["HELLO", "GOOD", "BYE"]
    
    # Find and print results
    for word in words:
        found = find_word(alphabet_soup, word)
        print(f"Word '{word}' {'found' if found else 'not found'} in the alphabet soup.")

if __name__ == "__main__":
    main()

